# Check IP_ADDRESS

- windows

```bash
ipconfig
```

- unix

```bash
ifconfig
```

## Script for find_ip_address

- unix

extract_ip.sh

```bash

#!/bin/bash

# Run ifconfig and store the output in a variable
ifconfig_output=$(ifconfig)
# echo $ifconfig_output

# Use grep to filter lines containing "flags=4163"
line=$(echo "$ifconfig_output" | grep "192.168.1.")
# echo $line

# Use grep again to extract the IP address matching "inet <IP_ADDRESS>"
ip_address=$(echo "$line" | grep -oE "inet ([0-9]{1,3}\.){3}[0-9]{1,3}" | awk '{print $2}')

# Print the extracted IP address
# echo "The IP address is: $ip_address"
# echo $ip_address

IFS=" " read -ra ip_arrays <<< "$ip_address"

target_ip=${ip_arrays[0]}

echo $target_ip
```

- windows

extract_ip.ps1

```ps1
# Run the ipconfig command and store the output in a variable
$ipconfig_output = ipconfig

# Use regex to find the IPv4 Address in the output
$ipv4_address = $ipconfig_output | Select-String -Pattern 'IPv4 Address.*: (\d+\.\d+\.\d+\.\d+)'

# Extract the IP address from the matched line
$ip_address = $ipv4_address.Matches.Groups[1].Value

# Print the extracted IP address
Write-Host "$ip_address"
```
